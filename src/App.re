/* Types */
type number =
  | Zero
  | One
  | Two
  | Three
  | Four
  | Five
  | Six
  | Seven
  | Eight
  | Nine;

type operation =
  | Add
  | Subtract
  | Multiply
  | Divide;

type symbol =
  | Digit(number)
  | Arithmetic(operation)
  | Dot;

type action =
  | Formulate(symbol)
  | Calculate
  | Clear;

type expression = list(symbol);

type state = {
  expression: expression,
  result: float
};

let initialState = {
  expression: [],
  result: 0.0
};

/* Helper functions */
let toString = ReasonReact.string;

let add = (a, b) => a +. b;
let subtract = (a, b) => a -. b;
let multiply = (a, b) => a *. b;
let divide = (a, b) => a /. b;

/* Stringify */
let mapOperationToString = operation =>
  switch operation {
  | Add => "+"
  | Subtract => "-"
  | Multiply => "x"
  | Divide => "/"
  };

let mapDigitToString = digit =>
  switch digit {
  | Zero => "0"
  | One => "1"
  | Two => "2"
  | Three => "3"
  | Four => "4"
  | Five => "5"
  | Six => "6"
  | Seven => "7"
  | Eight => "8"
  | Nine => "9"
  };

let mapSymbolToString = expression =>
  switch expression {
  | Arithmetic(operation) => mapOperationToString(operation)
  | Digit(number) => mapDigitToString(number)
  | Dot => "."
  }

let stringifyExpression = expression =>
  expression |> List.map(mapSymbolToString) |> String.concat("")

/* Computations */
let getNumbers = expression =>
  List.fold_right((symbol, acc) =>
    switch acc {
    | [] =>
      switch symbol {
      | Digit(n) => [mapDigitToString(n)]
      | _ => []
      }
    | [hd, ...tl] =>
      switch symbol {
      | Arithmetic(_) => ["", ...acc]
      | Digit(n) => [hd ++ (mapDigitToString(n)), ...tl]
      | Dot => [hd ++ ".", ...tl]
      }
    }
    , expression
    , []
  )
  |> List.map(float_of_string)

let getOperations = expression =>
  List.append(
    (List.filter(
      a =>
        switch a {
        | Arithmetic(_) => true
        | _ => false
        }
      , expression
      )
    )
    , [Arithmetic(Add)]
  )

let selectAdditonAndSubtraction = expression =>
  List.append(
    (List.filter(
      symbol =>
        switch symbol {
        | Arithmetic(Add) => true 
        | Arithmetic(Subtract) => true
        | _ => false
        }
      , expression
      )
    )
    , [Arithmetic(Multiply)]
  )

let exhaustMultiplicationAndDivision = expression =>
  List.fold_right2(
    (number, operation, acc) =>
      switch acc {
      | [] => [number]
      | [hd, ...tl] =>
        switch operation {
        | Arithmetic(Multiply) => [hd *. number, ...tl]
        | Arithmetic(Divide) => [hd /. number, ...tl]
        | _ => [number, ...acc]
        }
      }
      , (getNumbers(expression))
      , (getOperations(expression))
      , []
    )

let exhaustAddtionAndSubtraction = (expression, multiplied) =>
  List.fold_right2(
    (number, operation, acc) =>
      switch operation {
      | Arithmetic(Add) => (acc +. number)
      | Arithmetic(Subtract) => (acc -. number)
      | _ => number
      }
      , multiplied
      , (expression |> selectAdditonAndSubtraction)
      , 0.0
    )

let computeResult = expression =>
  exhaustAddtionAndSubtraction(expression, (exhaustMultiplicationAndDivision(expression)))

/* Update Expression */
let removeLeadingOperation = expression =>
  switch expression {
  | [] => []
  | [hd, ...tl] =>
    switch hd {
    | Arithmetic(_) => tl
    | _ => expression
    }
  }

let willZeroBeLeading = expression =>
  switch expression {
  | [] => true
  | [hd, ..._] =>
    switch hd {
    | Arithmetic(_) => true
    | _ => false
    }
  }

let rec hasDotPreceded = expression =>
  switch expression {
  | [] => false
  | [symbol] =>
    switch symbol {
    | Dot => true
    | _ => false
    }
  | [hd, ...tl] =>
    switch hd {
    | Dot => true
    | Arithmetic(_) => false
    | Digit(_) => hasDotPreceded(tl)
    }
  }

let update = (model, msg) =>
  switch msg {
  | Calculate => {expression: model.expression, result: model.expression |> removeLeadingOperation |> computeResult}
  | Clear => initialState
  | Formulate(symbol) =>
      {expression:
        switch symbol {
        | Digit(digit) =>
          switch digit {
          | Zero =>
            switch (willZeroBeLeading(model.expression)) {
            | true => model.expression
            | false => [symbol, ...model.expression]
            }
          | _ => [symbol, ...model.expression]
          }
        | Dot =>
            switch (hasDotPreceded(model.expression)) {
            | true => model.expression
            | false => [symbol, ...model.expression]
            }
        | Arithmetic(_) =>
            switch model.expression {
            | [] => []
            | [_] => [symbol, ...model.expression]
            | [hd, ...tl] =>
                switch hd {
                | Digit(_) => [symbol, ...model.expression]
                | Arithmetic(_) => [symbol, ...tl]
                | Dot => model.expression
                }
            }
        }
      , result: model.result}
  }

let component = ReasonReact.reducerComponent("App");

let make = (_children) => {
  ...component,
  initialState: () => initialState,
  reducer: (action: action, state: state) =>
    ReasonReact.Update(update(state, action)),
  render: ({state, send}) =>
    <div className="calculator">
      <div className="expression">
        <p>(List.rev(state.expression) |> stringifyExpression |> toString)</p>
        <div className="result">
          <p>(state.result |> string_of_float |> toString)</p>
        </div>
      </div>
      <button className="ac" onClick=(_evt => send(Clear))>("AC" |> toString)</button>
      <button className="divide" onClick=(_evt => send(Formulate(Arithmetic(Divide))))>("/" |> toString)</button>
      <button className="multiply" onClick=(_evt => send(Formulate(Arithmetic(Multiply))))>("X" |> toString)</button>
      <button className="subtract" onClick=(_evt => send(Formulate(Arithmetic(Subtract))))>("-" |> toString)</button>
      <button className="add" onClick=(_evt => send(Formulate(Arithmetic(Add))))>("+" |> toString)</button>
      <button className="equals" onClick=(_evt => send(Calculate))>("=" |> toString)</button>
      <button className="dot" onClick=(_evt => send(Formulate(Dot)))>("." |> toString)</button>
      <button className="zero" onClick=(_evt => send(Formulate(Digit(Zero))))>("0" |> toString)</button>
      <button className="one" onClick=(_evt => send(Formulate(Digit(One))))>("1" |> toString)</button>
      <button className="two" onClick=(_evt => send(Formulate(Digit(Two))))>("2" |> toString)</button>
      <button className="three" onClick=(_evt => send(Formulate(Digit(Three))))>("3" |> toString)</button>
      <button className="four" onClick=(_evt => send(Formulate(Digit(Four))))>("4" |> toString)</button>
      <button className="five" onClick=(_evt => send(Formulate(Digit(Five))))>("5" |> toString)</button>
      <button className="six" onClick=(_evt => send(Formulate(Digit(Six))))>("6" |> toString)</button>
      <button className="seven" onClick=(_evt => send(Formulate(Digit(Seven))))>("7" |> toString)</button>
      <button className="eight" onClick=(_evt => send(Formulate(Digit(Eight))))>("8" |> toString)</button>
      <button className="nine" onClick=(_evt => send(Formulate(Digit(Nine))))>("9" |> toString)</button>
    </div>
};
