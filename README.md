This repository implements a simple calculator using [ReasonML](https://reasonml.github.io/) and [ReasonReact](https://reasonml.github.io/reason-react/).

The calculator characteristics loosely follows the example implementation listed in [freeCodeCamp's calculator exercise](https://learn.freecodecamp.org/front-end-libraries/front-end-libraries-projects/build-a-javascript-calculator/).

#### Installation and Build
1. `yarn install`.
2. `yarn build`.
3. `yarn build:webpack`.

#### Usage
Load `index.html` in a browser with CSS-grid support like Chromium or Firefox.
